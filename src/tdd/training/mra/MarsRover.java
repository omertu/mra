package tdd.training.mra;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MarsRover {
	private List<String> inputPlanetObstacles = new ArrayList<>();
	private int inputPlanetX ;
	private int inputPlanetY ;
	Rover activeRover;

	
	private class Rover {
		private int roverX;
		private int roverY;
		private char facingDirection;
		
		private Rover(int roverX, int roverY, char facingDirection) {
			this.roverX = roverX;
			this.roverY = roverY;
			this.facingDirection = facingDirection;
		}
		
		private void move(char command) throws MarsRoverException{
			if (command == 'f') {
				roverY++;
				facingDirection='N';
			} else if (command == 'b') {
				roverY--;
				facingDirection='S';
			} else if (command == 'l') {
				facingDirection='W';
			} else if (command == 'r') {
				facingDirection='E';
			} else 
				throw new MarsRoverException("Invalid Command");
		}
		
		private String getStatus() {
			StringBuilder status = new StringBuilder("(");
			status.append(roverX);
			status.append(',');
			status.append(roverY);
			status.append(',');
			status.append(facingDirection);
			status.append(')');
			
			return status.toString();

			}
		
	}

	
	
	/**
	 * It initializes the rover at the coordinates (0,0), facing North, on a planet
	 * (represented as a grid with x and y coordinates) containing obstacles.
	 * 
	 * @param planetX         The x dimension of the planet.
	 * @param planetY         The y dimension of the planet.
	 * @param planetObstacles The obstacles on the planet. Each obstacle is a string
	 *                        (without white spaces) formatted as follows:
	 *                        "(oi_x,oi_y)". <code>null</code> if the planet does
	 *                        not contain obstacles.
	 * 
	 * @throws MarsRoverException
	 */
	public MarsRover(int planetX, int planetY, List<String> planetObstacles) throws MarsRoverException {
		inputPlanetX = planetX;
		inputPlanetY = planetY;
		inputPlanetObstacles = planetObstacles;
		if (checkObstaclesOutOfPlanet())
			throw new MarsRoverException("Obstacle out of grid");
		activeRover = new Rover(0,0,'N');
		
	}
	

	/**
	 * It returns whether, or not, the planet (where the rover moves) contains an
	 * obstacle in a cell.
	 * 
	 * @param x The x coordinate of the cell
	 * @param y The y coordinate of the cell
	 * @return <true> if the cell contains an obstacle, <false> otherwise.
	 * @throws MarsRoverException
	 */
	public boolean planetContainsObstacleAt(int x, int y) throws MarsRoverException {
		String formattedObstacleAt = "(" + String.valueOf(x) + "," + String.valueOf(y) + ")";
		return inputPlanetObstacles.contains(formattedObstacleAt);
	}

	/**
	 * It lets the rover move on the planet according to a command string. The
	 * return string contains the new position of the rover, its direction, and the
	 * obstacles it has encountered while moving on the planet (if any).
	 * 
	 * @param commandString A string that can contain a single command -- i.e. "f"
	 *                      (forward), "b" (backward), "l" (left), or "r" (right) --
	 *                      or a combination of single commands.
	 * @return The return string that contains the position and direction of the
	 *         rover, and the obstacles the rover has encountered while moving on
	 *         the planet (if any). The return string (without white spaces) has the
	 *         following format: "(x,y,dir)(o1_x,o1_y)(o2_x,o2_y)...(on_x,on_y)". x
	 *         and y define the new position of the rover while dir represents its
	 *         direction (i.e., N, S, W, or E). Finally, oi_x and oi_y are the
	 *         coordinates of the i-th encountered obstacle.
	 * @throws MarsRoverException
	 */
	public String executeCommand(String commandString) throws MarsRoverException {
		if (commandString.isEmpty())
			return activeRover.getStatus();
		else {
			char[] commands = commandString.toCharArray();
			for (char c : commands) {
				activeRover.move(c);
			}
			return activeRover.getStatus();
		}
	}

	private boolean checkObstaclesOutOfPlanet() {
		for(String s : inputPlanetObstacles) {
		    int[] obstacleCoordinates = Arrays.stream(s.substring(1, s.length()-1).split(",")).mapToInt(Integer::parseInt).toArray();
		    if ((obstacleCoordinates[0]>inputPlanetX) || (obstacleCoordinates[1]>inputPlanetY))
		    	return true;
		}
		return false;
	}

	}

