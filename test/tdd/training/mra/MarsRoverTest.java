package tdd.training.mra;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;


public class MarsRoverTest {

	@Test
	public void testPlanetIsCorrect() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("(4,7)");
		planetObstacles.add("(2,3)");
		MarsRover rover = new MarsRover(10, 10, planetObstacles);
		
		assertTrue(rover.planetContainsObstacleAt(4, 7));
		assertTrue(rover.planetContainsObstacleAt(2, 3));
		
	}
	
	@Test(expected = MarsRoverException.class)
	public void testObstacleOutOfPlanet() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("(4,7)");
		planetObstacles.add("(11,3)");
		MarsRover rover = new MarsRover(10, 10, planetObstacles);

	}
	
	@Test 
	public void testEmptyCommandString() throws MarsRoverException{
		List<String> planetObstacles = new ArrayList<>();
		MarsRover rover = new MarsRover(10, 10, planetObstacles);
		String newStatus = new String(rover.executeCommand(""));
		assertEquals("(0,0,N)", newStatus);
		
	}
	
	@Test 
	public void testTurningCommand() throws MarsRoverException{
		List<String> planetObstacles = new ArrayList<>();
		MarsRover rover = new MarsRover(10, 10, planetObstacles);
		assertEquals("(0,0,E)", rover.executeCommand("r"));
		
	}
	
	@Test 
	public void testMovingForward() throws MarsRoverException{
		List<String> planetObstacles = new ArrayList<>();
		MarsRover rover = new MarsRover(10, 10, planetObstacles);
		String Command = "f";
		assertEquals("(0,1,N)", rover.executeCommand(Command));
		
	}
	

}
